package com.stuff.simplewebapp01;

import org.springframework.context.annotation.Configuration;

import com.vaadin.spring.annotation.EnableVaadin;

@Configuration
@EnableVaadin
public class WebAppConfiguration {
}