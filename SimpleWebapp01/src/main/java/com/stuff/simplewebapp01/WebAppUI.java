package com.stuff.simplewebapp01;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

@SpringUI
public class WebAppUI extends UI {

	@Override
	protected void init(VaadinRequest request) {
		setContent(new Label("Vaadin webapp"));
	}
}
