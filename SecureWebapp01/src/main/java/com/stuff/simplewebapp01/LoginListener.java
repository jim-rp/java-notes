package com.stuff.simplewebapp01;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class LoginListener implements ApplicationListener <InteractiveAuthenticationSuccessEvent> {

	@Autowired
	private HttpSession session;
	
	@Override
	public void onApplicationEvent(InteractiveAuthenticationSuccessEvent event) {
		UserDetails details = (UserDetails) event.getAuthentication().getPrincipal();
		
		session.toString();
		session.setAttribute("loginName", details.getName());
		System.out.println (session.getAttribute("loginName"));
	}
}
